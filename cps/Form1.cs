﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace awesomeness
{
    
    public partial class frmMain : Form
    {
        
        const int ThePopulation = 6;
        bool[,] graph = new bool[ThePopulation, ThePopulation] { 
                                                { false, true, false, true, false, false }, 
                                                { true, false, true, true, true, false }, 
                                                { false, true, false, false, true, true }, 
                                                { true, true, false, false, true, false }, 
                                                { false, true, true, true, false, true },
                                                { false, false, true, false, true, false }
                                                };
       
        public int connections=0;
        public int[] chromosome1 = new int[ThePopulation]; 
        public int[] chromosome2 = new int[ThePopulation];
        public int[] chromosome3 = new int[ThePopulation];
        public int[] chromosome4 = new int[ThePopulation];
        public int[] chromosome5 = new int[ThePopulation];
        public int[] chromosome6 = new int[ThePopulation];
        public int[] chromosome7 = new int[ThePopulation]; // kid
        public int[] chromosome8 = new int[ThePopulation]; // kid
        public int[] theTwoBestChromosomes; // use numberToCromosome() ..
        Random myRandom = new Random();
        int count = 1;
        String textBox1Text = "";
            
        public frmMain()
        {
            InitializeComponent();
            textBox1.Text += "Program Initialized" + "\r\n";
           // int [,] graph = new int[6,6];
            for (int i = 1; i <= ThePopulation; i++) 
            {
                for (int j = i; j <= ThePopulation; j++)
                {
                    if (graph[i-1, j-1] == true)
                    { // connection between i and j 
                        connections++;
                    }                
                }
            }

            textBox1.Text += "Making new parents.." + "\r\n";
            makeNewParents();

            textBox1Text += "Chromosome1: " + ""; writeChromosome(chromosome1); textBox1Text += " Health: " + checkSatisfaction(chromosome1) + "\r\n";
            textBox1Text += "Chromosome2: " + ""; writeChromosome(chromosome2); textBox1Text += " Health: " + checkSatisfaction(chromosome2) + "\r\n";
            textBox1Text += "Chromosome3: " + ""; writeChromosome(chromosome3); textBox1Text += " Health: " + checkSatisfaction(chromosome3) + "\r\n";
            textBox1Text += "Chromosome4: " + ""; writeChromosome(chromosome4); textBox1Text += " Health: " + checkSatisfaction(chromosome4) + "\r\n";
            textBox1Text += "Chromosome5: " + ""; writeChromosome(chromosome5); textBox1Text += " Health: " + checkSatisfaction(chromosome5) + "\r\n";
            textBox1Text += "Chromosome6: " + ""; writeChromosome(chromosome6); textBox1Text += " Health: " + checkSatisfaction(chromosome6) + "\r\n";
            textBox1.Text += textBox1Text; textBox1Text = "";

            int theBest = findTheBestChromosome();
            textBox1.Text += "findTheBestChromosome() = chromosome" + theBest + "\r\n";
            
            paintints(numberToCromosome(theBest));


        }

        public void eliminateTheTwoWorst() {
            // find two worst
            int [] theTwoWorst = findTheTwoWorstChromosomes();
            int whileCounter = 0;
            int[] worst1 = new int[ThePopulation];
            int[] worst2 = new int[ThePopulation];

            Array.Sort(theTwoWorst);
            textBox1Text += "-- WORST: Chromosome" + theTwoWorst[0] + "\r\n";
            textBox1Text += "-- WORST: Chromosome" + theTwoWorst[1] + "\r\n";
            while (whileCounter < 2)
            {

                switch (theTwoWorst[whileCounter])
                {
                        // welcome to the hell of shaddow copying and switches..
                    case 1:
                        
                        if ((whileCounter == 0))
                        {
                            Array.Copy(chromosome1, worst1, ThePopulation);
                        }

                        else if (whileCounter == 1)
                        {
                            Array.Copy(chromosome1, worst2, ThePopulation);
                        }
                        break;
                    case 2:
                        if ((whileCounter == 0))
                        {
                            Array.Copy(chromosome2, worst1, ThePopulation);
                        }

                        else if (whileCounter == 1)
                        {
                            Array.Copy(chromosome2, worst2, ThePopulation);

                        }
                        break;
                    case 3:
                        if ((whileCounter == 0))
                        {
                        Array.Copy(chromosome3, worst1, ThePopulation);

                        }

                        else if (whileCounter == 1)
                        {
                        Array.Copy(chromosome3, worst2, ThePopulation);

                        }
                        break;
                    case 4:
                        if ((whileCounter == 0))
                        {
                        Array.Copy(chromosome4, worst1, ThePopulation);

                        }

                        else if (whileCounter == 1)
                        {
                        Array.Copy(chromosome4, worst2, ThePopulation);

                        }
                        break;
                    case 5:
                        if ((whileCounter == 0))
                        {
                        Array.Copy(chromosome5, worst1, ThePopulation);

                        }

                        else if (whileCounter == 1)
                        {
                        Array.Copy(chromosome5, worst2, ThePopulation);

                        }
                        break;
                    case 6:
                        if ((whileCounter == 0))
                        {
                        Array.Copy(chromosome6, worst1, ThePopulation);

                        }

                        else if (whileCounter == 1)
                        {
                        Array.Copy(chromosome6, worst2, ThePopulation);

                        }
                        break;
                    case 7:
                        if ((whileCounter == 0))
                        {
                        Array.Copy(chromosome7, worst1, ThePopulation);

                        }

                        else if (whileCounter == 1)
                        {
                        Array.Copy(chromosome7, worst2, ThePopulation);

                        }
                        break;
                    case 8:
                        if ((whileCounter == 0))
                        {
                        Array.Copy(chromosome8, worst1, ThePopulation);

                        }

                        else if (whileCounter == 1)
                        {
                        Array.Copy(chromosome8, worst2, ThePopulation);
                            
                        }
                        break;
                    default:
                        textBox1Text += "(Err) WORST: Switch out of bound: " + theTwoWorst[whileCounter] + "\r\n";
                        break;
                }
                whileCounter++;
            }
            switch (theTwoWorst[0]) // Sw2
            {
                case 1:
                    Array.Copy(chromosome7, chromosome1, ThePopulation);
                    break;
                case 2:
                    Array.Copy(chromosome7, chromosome2, ThePopulation);
                    break;
                case 3:
                    Array.Copy(chromosome7, chromosome3, ThePopulation);
                    break;
                case 4:
                    Array.Copy(chromosome7, chromosome4, ThePopulation);
                    break;
                case 5:
                    Array.Copy(chromosome7, chromosome5, ThePopulation);
                    break;
                case 6:
                    Array.Copy(chromosome7, chromosome6, ThePopulation);
                    break;
                case 7:
                    Array.Copy(chromosome7, chromosome7, ThePopulation);
                    break;
                default:
                    textBox1Text += "(Err) WORST: Sw2 out of bound: " + theTwoWorst[0] + "\r\n";
                    break;
            }
            switch (theTwoWorst[1]) // Sw3
            {
                case 2:
                    Array.Copy(chromosome8, chromosome2, ThePopulation);
                    break;
                case 3:
                    Array.Copy(chromosome8, chromosome3, ThePopulation);
                    break;
                case 4:
                    Array.Copy(chromosome8, chromosome4, ThePopulation);
                    break;
                case 5:
                    Array.Copy(chromosome8, chromosome5, ThePopulation);
                    break;
                case 6:
                    Array.Copy(chromosome8, chromosome6, ThePopulation);
                    break;
                case 7:
                    Array.Copy(chromosome8, chromosome7, ThePopulation);
                    break;
                case 8:
                    Array.Copy(chromosome8, chromosome8, ThePopulation);
                    break;
                default:
                    textBox1Text += "(Err) WORST: Sw3 out of bound: " + theTwoWorst[1] + "\r\n";
                    break;
            }
            Array.Copy(worst1, chromosome7, ThePopulation);
            Array.Copy(worst2, chromosome8, ThePopulation);
        }

        public void twoPointsCrossover() {
            int[] theParents = chooseParentChromosomes(); // get the two best chromosomes (1-ThePopulation) (not including any childs)

            textBox1Text += "chooseParentChromosomes(): " + theParents[0] + "," + theParents[1] + "\r\n";

            int[] parent1 = new int[ThePopulation];
            Array.Copy(numberToCromosome(theParents[0]), parent1, ThePopulation);
            int[] parent2 = new int[ThePopulation];
            Array.Copy(numberToCromosome(theParents[1]), parent2, ThePopulation);
           
            Array.Copy(parent1, chromosome8, ThePopulation); // swap 
            Array.Copy(parent2, chromosome7, ThePopulation); // swap

            int[] crossoverOperators = new int[2] { 0, 0 };
            while (crossoverOperators[0] == crossoverOperators[1])
            {
                crossoverOperators[0] = myRandom.Next(0, 6);
                crossoverOperators[1] = myRandom.Next(0, 6);
            }
            Array.Sort(crossoverOperators);

            textBox1Text += "-- crossoverOperators: " + crossoverOperators[0] + "," + crossoverOperators[1] + "\r\n";

            // swap mid part [   |***|   ]
            for (int i = crossoverOperators[0]; i <= crossoverOperators[1]; i++)
            {
                chromosome7[i] = parent1[i];
                chromosome8[i] = parent2[i];
            }

            textBox1Text += "-- Kid: Chromosome7: " + ""; writeChromosome(chromosome7); textBox1Text += " Health: " + checkSatisfaction(chromosome7) + "\r\n";
            textBox1Text += "-- Kid: Chromosome8: " + ""; writeChromosome(chromosome8); textBox1Text += " Health: " + checkSatisfaction(chromosome8) + "\r\n";
           
        }

        public int[] chooseParentChromosomes()
        {
            // choose two parents from chromosome 1-ThePopulation
            int[] chromosomeSatisfaction = new int[ThePopulation];
            int[] tempToReturn = new int[2] {0,0};
            for (int i = 0; i <= (ThePopulation-1); i++)
            {
                chromosomeSatisfaction[i] = checkSatisfaction(numberToCromosome(i + 1));
            }
            int whileCounter = 0;

            while (whileCounter < 2)
            {
                //tempToReturn[whileCounter] = 0;
                for (int j = 0; j <= (ThePopulation - 1); j++)
                {
                    if (chromosomeSatisfaction[j] > chromosomeSatisfaction[tempToReturn[whileCounter]]) { tempToReturn[whileCounter] = j; }
                }
                chromosomeSatisfaction[tempToReturn[whileCounter]] = 0; // hide highest value -> lets find nest highest value
                whileCounter++;
            }
            tempToReturn[0]++;// = tempToReturn[0]++;
            tempToReturn[1]++;// = tempToReturn[1]++;
            return tempToReturn; // 1 = chromosone1

        }
        public int[] findTheTwoWorstChromosomes()
        {
            // choose two parents from chromosome 1-ThePopulation
            int[] chromosomeSatisfaction = new int[(ThePopulation + 2)]; 
            int[] tempToReturn = new int[2] {1,1};
            for (int i = 0; i <= (ThePopulation + 1); i++)
            {
                chromosomeSatisfaction[i] = checkSatisfaction(numberToCromosome(i+1));
            }
            int whileCounter = 0;

            while (whileCounter < 2)
            {
                for (int j = 0; j <= (ThePopulation + 1); j++)
                {
                    if (chromosomeSatisfaction[j] < chromosomeSatisfaction[tempToReturn[whileCounter]]) { tempToReturn[whileCounter] = j; }
                }
                //if (chromosomeSatisfaction[0] < chromosomeSatisfaction[tempToReturn[whileCounter]]) { tempToReturn[whileCounter] = 0; }
                chromosomeSatisfaction[tempToReturn[whileCounter]] = 100; // hide lowest value -> lets find nest lowest value
                whileCounter++;
            }
            tempToReturn[0]++;
            tempToReturn[1]++;
            return tempToReturn; // 1 = chromosone1

        }
public int findTheBestChromosome()
{
    // choose two parents from chromosome 1-ThePopulation
    int[] chromosomeSatisfaction = new int[ThePopulation] ;
    int tempToReturn = 0;
    for (int i = 0; i <= (ThePopulation - 1); i++)
    {
        chromosomeSatisfaction[i] = checkSatisfaction(numberToCromosome(i + 1));
    }
    for (int j = 0; j <= (ThePopulation - 1); j++)
    {
        if (chromosomeSatisfaction[j] > chromosomeSatisfaction[tempToReturn]) { tempToReturn = j; }
    }
    tempToReturn++;
    return tempToReturn; // 1 = chromosone1
}

        public int[] numberToCromosome(int theNumber)
        {
            switch (theNumber)
            {
                case 1:
                    return chromosome1;
                    
                case 2:
                    return chromosome2;
                    
                case 3:
                    return chromosome3;
                    
                case 4:
                    return chromosome4;
                    
                case 5:
                    return chromosome5;
                    
                case 6:
                    return chromosome6;
                    
                case 7:
                    return chromosome7;
                    
                case 8:
                    return chromosome8;
                    
                default:

                    textBox1Text += "(err) numberToCromosome() out of bounds: " + theNumber + ". Assuming 1..\r\n";
                    return chromosome1;
            }
        }

        private int[] InitializeInitialPopulation()
        {
            int[] tmpPopulation = new int[ThePopulation];

            for (int i = 1; i <= ThePopulation; i++)
            {
                int randomNumber = myRandom.Next(1, 4);
                tmpPopulation[i - 1] = randomNumber;
            }
            return tmpPopulation;
        }
        public Color numberToColor(int number)
        {
            Color theColor;
            switch (number)
            {
                case 1:
                    theColor = Color.Red;
                    break;
                case 2:
                    theColor  = Color.Blue;
                    break;
                case 3:
                    theColor = Color.Green;
                    break;
                default:
                    theColor = Color.Black;
                    break;
            }
            return theColor;
        }

        public int checkSatisfaction(int[] gimmeAnintArray) {
            int satisfactionNumber = 0;
            for (int i = 1; i <= ThePopulation; i++) 
            {
                for (int j = i; j <= ThePopulation; j++)
                {
                    if (graph[i-1, j-1] == true)
                    { // connection between i and j 
                        if (gimmeAnintArray[i-1] == gimmeAnintArray[j-1])
                        {
                            satisfactionNumber++;
                        }
                    }                
                }
            }
            return (connections - satisfactionNumber); 
        }

        public void makeNewParents()
        {
            Array.Copy(InitializeInitialPopulation(), chromosome1, ThePopulation);
            Array.Copy(InitializeInitialPopulation(), chromosome2, ThePopulation);
            Array.Copy(InitializeInitialPopulation(), chromosome3, ThePopulation);
            Array.Copy(InitializeInitialPopulation(), chromosome4, ThePopulation);
            Array.Copy(InitializeInitialPopulation(), chromosome5, ThePopulation);
            Array.Copy(InitializeInitialPopulation(), chromosome6, ThePopulation);
        }

        public void writeChromosome(int[] gimmeAnintArray)
        {
            textBox1Text += "{" + " ";
            for (int i = 1; i <= ThePopulation; i++)
            {
                textBox1Text += "" + gimmeAnintArray[i - 1] + " ";
            }

            textBox1Text += "}"; // +"\r\n";
        }

        public void paintints(int[] gimmeAnintArray)
        {
            for (int i = 1; i <= ThePopulation; i++)
            {
                switch (i)
                {
                    case 1:
                        shapeV1.FillColor = numberToColor(gimmeAnintArray[i - 1]);
                        break;
                    case 2:
                        shapeV2.FillColor = numberToColor(gimmeAnintArray[i - 1]);
                        break;
                    case 3:
                        shapeV3.FillColor = numberToColor(gimmeAnintArray[i - 1]);
                        break;
                    case 4:
                        shapeV4.FillColor = numberToColor(gimmeAnintArray[i - 1]);
                        break;
                    case 5:
                        shapeV5.FillColor = numberToColor(gimmeAnintArray[i - 1]);
                        break;
                    case 6:
                        shapeV6.FillColor = numberToColor(gimmeAnintArray[i - 1]);
                        break;
                    default:
                        shapeV1.FillColor = Color.Pink; // wtf, error happening..
                        break;
                }
            }
            label1.Text += "  -  Health: " + (checkSatisfaction(gimmeAnintArray));
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            // 
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            label1.Text = "chromosome1";
            paintints(chromosome1);
           // checkSatisfaction(parent1);
        }
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            label1.Text = "chromosome2";
            paintints(chromosome2);
            //checkSatisfaction(parent2);
        }
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            label1.Text = "chromosome3";
            paintints(chromosome3);
           // checkSatisfaction(parent3);
        }
        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            label1.Text = "chromosome4";
            paintints(chromosome4);
            //checkSatisfaction(parent4);
        }
        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            label1.Text = "chromosome5";
            paintints(chromosome5);
            //checkSatisfaction(parent5);
        }
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            label1.Text = "chromosome6";
            paintints(chromosome6);
           // checkSatisfaction(parent6);
        }

        private void makeNewParentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            makeNewParents();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Terje Nilsen - fall, 2013 - terje.nilsen@student.hive.no");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int theBest;
            // make kids:
            
                count = 1;
                //button1.Text = "Start" + count;
                textBox1.Text += "Starting..\r\n";
            while (count <= 50)
            {
                //button1.Text = "Count: " + count;

                textBox1Text += "twoPointsCrossover() ("+count+")\r\n";
                twoPointsCrossover(); //chromosome7-8 = the kids

                // we got kids, now what?
                textBox1Text += "eliminateTheTwoWorst()\r\n";
                eliminateTheTwoWorst(); // reorder, delete chromosome7-8
               

                textBox1Text += "Chromosome1: " + ""; writeChromosome(chromosome1); textBox1Text += " Health: " + checkSatisfaction(chromosome1) + "\r\n";
                textBox1Text += "Chromosome2: " + ""; writeChromosome(chromosome2); textBox1Text += " Health: " + checkSatisfaction(chromosome2) + "\r\n";
                textBox1Text += "Chromosome3: " + ""; writeChromosome(chromosome3); textBox1Text += " Health: " + checkSatisfaction(chromosome3) + "\r\n";
                textBox1Text += "Chromosome4: " + ""; writeChromosome(chromosome4); textBox1Text += " Health: " + checkSatisfaction(chromosome4) + "\r\n";
                textBox1Text += "Chromosome5: " + ""; writeChromosome(chromosome5); textBox1Text += " Health: " + checkSatisfaction(chromosome5) + "\r\n";
                textBox1Text += "Chromosome6: " + ""; writeChromosome(chromosome6); textBox1Text += " Health: " + checkSatisfaction(chromosome6) + "\r\n";


                theBest = findTheBestChromosome();
                textBox1Text += "findTheBestChromosome() = chromosome" + theBest + "\r\n";
                /*label1.Text = "";
                paintints(numberToCromosome(theBest));
                label1.Text = "(showing the best): chromosome" + theBest + " " + label1.Text;
                 * */
                count++;
            }

            theBest = findTheBestChromosome();
            label1.Text = "";
            paintints(numberToCromosome(theBest));
            label1.Text = "(showing the best): chromosome" + theBest + " " + label1.Text;

            textBox1Text += "Chromosome1: " + ""; writeChromosome(chromosome1); textBox1Text += " Health: " + checkSatisfaction(chromosome1) + "\r\n";
            textBox1Text += "Chromosome2: " + ""; writeChromosome(chromosome2); textBox1Text += " Health: " + checkSatisfaction(chromosome2) + "\r\n";
            textBox1Text += "Chromosome3: " + ""; writeChromosome(chromosome3); textBox1Text += " Health: " + checkSatisfaction(chromosome3) + "\r\n";
            textBox1Text += "Chromosome4: " + ""; writeChromosome(chromosome4); textBox1Text += " Health: " + checkSatisfaction(chromosome4) + "\r\n";
            textBox1Text += "Chromosome5: " + ""; writeChromosome(chromosome5); textBox1Text += " Health: " + checkSatisfaction(chromosome5) + "\r\n";
            textBox1Text += "Chromosome6: " + ""; writeChromosome(chromosome6); textBox1Text += " Health: " + checkSatisfaction(chromosome6) + "\r\n";
            textBox1.Text += textBox1Text; textBox1Text = "";
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            count = 0;
            textBox1.Clear();
            textBox1.Text += "Making new parents.." + "\r\n";
            makeNewParents();


            textBox1Text += "Chromosome1: " + ""; writeChromosome(chromosome1); textBox1Text += " Health: " + checkSatisfaction(chromosome1) + "\r\n";
            textBox1Text += "Chromosome2: " + ""; writeChromosome(chromosome2); textBox1Text += " Health: " + checkSatisfaction(chromosome2) + "\r\n";
            textBox1Text += "Chromosome3: " + ""; writeChromosome(chromosome3); textBox1Text += " Health: " + checkSatisfaction(chromosome3) + "\r\n";
            textBox1Text += "Chromosome4: " + ""; writeChromosome(chromosome4); textBox1Text += " Health: " + checkSatisfaction(chromosome4) + "\r\n";
            textBox1Text += "Chromosome5: " + ""; writeChromosome(chromosome5); textBox1Text += " Health: " + checkSatisfaction(chromosome5) + "\r\n";
            textBox1Text += "Chromosome6: " + ""; writeChromosome(chromosome6); textBox1Text += " Health: " + checkSatisfaction(chromosome6) + "\r\n";
            textBox1.Text += textBox1Text; textBox1Text = "";
            int theBest = findTheBestChromosome();
            textBox1.Text += "findTheBestChromosome() = chromosome" + theBest + "\r\n";
            paintints(numberToCromosome(theBest));
        }

    }
}
