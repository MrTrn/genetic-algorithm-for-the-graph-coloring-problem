﻿namespace awesomeness
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.shapeC6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeC5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeV3 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.shapeC7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeV6 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.shapeV5 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.shapeV2 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.shapeC2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeC8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeC3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeC9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeC4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeV1 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.shapeC1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeV4 = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeNewParentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.shapeContainer1);
            this.panel1.Location = new System.Drawing.Point(12, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 348);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Health: ...";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.shapeC6,
            this.shapeC5,
            this.shapeV3,
            this.shapeC7,
            this.shapeV6,
            this.shapeV5,
            this.shapeV2,
            this.shapeC2,
            this.shapeC8,
            this.shapeC3,
            this.shapeC9,
            this.shapeC4,
            this.shapeV1,
            this.shapeC1,
            this.shapeV4});
            this.shapeContainer1.Size = new System.Drawing.Size(524, 348);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // shapeC6
            // 
            this.shapeC6.Cursor = System.Windows.Forms.Cursors.Default;
            this.shapeC6.Name = "shapeC6";
            this.shapeC6.X1 = 222;
            this.shapeC6.X2 = 315;
            this.shapeC6.Y1 = 109;
            this.shapeC6.Y2 = 241;
            // 
            // shapeC5
            // 
            this.shapeC5.Cursor = System.Windows.Forms.Cursors.Default;
            this.shapeC5.Name = "shapeC5";
            this.shapeC5.X1 = 240;
            this.shapeC5.X2 = 302;
            this.shapeC5.Y1 = 90;
            this.shapeC5.Y2 = 106;
            // 
            // shapeV3
            // 
            this.shapeV3.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shapeV3.Location = new System.Drawing.Point(311, 95);
            this.shapeV3.Name = "shapeV3";
            this.shapeV3.Size = new System.Drawing.Size(42, 42);
            // 
            // shapeC7
            // 
            this.shapeC7.Cursor = System.Windows.Forms.Cursors.Default;
            this.shapeC7.Name = "shapeC7";
            this.shapeC7.X1 = 362;
            this.shapeC7.X2 = 448;
            this.shapeC7.Y1 = 128;
            this.shapeC7.Y2 = 182;
            // 
            // shapeV6
            // 
            this.shapeV6.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shapeV6.Location = new System.Drawing.Point(449, 175);
            this.shapeV6.Name = "shapeV6";
            this.shapeV6.Size = new System.Drawing.Size(42, 42);
            // 
            // shapeV5
            // 
            this.shapeV5.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shapeV5.Location = new System.Drawing.Point(313, 248);
            this.shapeV5.Name = "shapeV5";
            this.shapeV5.Size = new System.Drawing.Size(42, 42);
            // 
            // shapeV2
            // 
            this.shapeV2.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shapeV2.Location = new System.Drawing.Point(189, 59);
            this.shapeV2.Name = "shapeV2";
            this.shapeV2.Size = new System.Drawing.Size(42, 42);
            // 
            // shapeC2
            // 
            this.shapeC2.Name = "shapeC2";
            this.shapeC2.X1 = 75;
            this.shapeC2.X2 = 134;
            this.shapeC2.Y1 = 171;
            this.shapeC2.Y2 = 236;
            // 
            // shapeC8
            // 
            this.shapeC8.Cursor = System.Windows.Forms.Cursors.Default;
            this.shapeC8.Name = "shapeC8";
            this.shapeC8.X1 = 366;
            this.shapeC8.X2 = 449;
            this.shapeC8.Y1 = 260;
            this.shapeC8.Y2 = 213;
            // 
            // shapeC3
            // 
            this.shapeC3.Cursor = System.Windows.Forms.Cursors.Default;
            this.shapeC3.Name = "shapeC3";
            this.shapeC3.X1 = 184;
            this.shapeC3.X2 = 303;
            this.shapeC3.Y1 = 262;
            this.shapeC3.Y2 = 270;
            // 
            // shapeC9
            // 
            this.shapeC9.Cursor = System.Windows.Forms.Cursors.Default;
            this.shapeC9.Name = "shapeC9";
            this.shapeC9.X1 = 335;
            this.shapeC9.X2 = 332;
            this.shapeC9.Y1 = 146;
            this.shapeC9.Y2 = 234;
            // 
            // shapeC4
            // 
            this.shapeC4.Cursor = System.Windows.Forms.Cursors.Default;
            this.shapeC4.Name = "shapeC4";
            this.shapeC4.X1 = 201;
            this.shapeC4.X2 = 166;
            this.shapeC4.Y1 = 112;
            this.shapeC4.Y2 = 224;
            // 
            // shapeV1
            // 
            this.shapeV1.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shapeV1.Location = new System.Drawing.Point(33, 127);
            this.shapeV1.Name = "shapeV1";
            this.shapeV1.Size = new System.Drawing.Size(42, 42);
            // 
            // shapeC1
            // 
            this.shapeC1.Name = "shapeC1";
            this.shapeC1.X1 = 86;
            this.shapeC1.X2 = 178;
            this.shapeC1.Y1 = 133;
            this.shapeC1.Y2 = 94;
            // 
            // shapeV4
            // 
            this.shapeV4.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.shapeV4.Location = new System.Drawing.Point(134, 236);
            this.shapeV4.Name = "shapeV4";
            this.shapeV4.Size = new System.Drawing.Size(42, 42);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programToolStripMenuItem,
            this.updateToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1093, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.toolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.programToolStripMenuItem.Text = "Program";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(132, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.parentsToolStripMenuItem});
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.updateToolStripMenuItem.Text = "View";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // parentsToolStripMenuItem
            // 
            this.parentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.makeNewParentsToolStripMenuItem,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8});
            this.parentsToolStripMenuItem.Name = "parentsToolStripMenuItem";
            this.parentsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.parentsToolStripMenuItem.Text = "Chromosome";
            // 
            // makeNewParentsToolStripMenuItem
            // 
            this.makeNewParentsToolStripMenuItem.Name = "makeNewParentsToolStripMenuItem";
            this.makeNewParentsToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.makeNewParentsToolStripMenuItem.Text = "Make fresh (new)";
            this.makeNewParentsToolStripMenuItem.Click += new System.EventHandler(this.makeNewParentsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D1)));
            this.toolStripMenuItem3.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem3.Text = "1";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D2)));
            this.toolStripMenuItem4.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem4.Text = "2";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D3)));
            this.toolStripMenuItem5.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem5.Text = "3";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D4)));
            this.toolStripMenuItem6.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem6.Text = "4";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D5)));
            this.toolStripMenuItem7.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem7.Text = "5";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D6)));
            this.toolStripMenuItem8.Size = new System.Drawing.Size(165, 22);
            this.toolStripMenuItem8.Text = "6";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(542, 56);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(523, 348);
            this.textBox1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(98, 27);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "New Population";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 431);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "CPS: Coloring Problem, three colors";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC6;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC5;
        private Microsoft.VisualBasic.PowerPacks.OvalShape shapeV3;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC7;
        private Microsoft.VisualBasic.PowerPacks.OvalShape shapeV6;
        private Microsoft.VisualBasic.PowerPacks.OvalShape shapeV5;
        private Microsoft.VisualBasic.PowerPacks.OvalShape shapeV2;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC2;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC8;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC3;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC9;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC4;
        private Microsoft.VisualBasic.PowerPacks.OvalShape shapeV1;
        private Microsoft.VisualBasic.PowerPacks.LineShape shapeC1;
        private Microsoft.VisualBasic.PowerPacks.OvalShape shapeV4;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem makeNewParentsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
    }
}

