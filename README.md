# Genetic Algorithm for the Graph Coloring Problem #
Three colors.

Second assignment for the DA-OPT3900 Algorithm Optimization course at Buskerud and Vestfold Universty College, fall 2013. 

## Technologies / topics ##
* Graph Coloring Problem
* Genetic Algorithm (Chromosome)
* Mutations and crossover
* C# Drawing 

## Rules algorithm ##
- Select parents of the top 2 in the population 
- Must have two different points to Two Points Crossover 
- Initial population = 6 
- Stop after 50 iterations

## The program ##
![forbedring77-8.png](https://bitbucket.org/repo/6o8x8R/images/2891228534-forbedring77-8.png)

## Assignment ##
Implement a genetic algorithm for the graph coloring problem ( three colours ).
                The size of the initial population should be between between 6 and 20.
                Use the 2­points crossover
Test your genetic algorithm on some random graphs that you generate.
You should answer these two questions :
    1: Does the size of the initial population influence the solution quality ?
    2: Does the number of generations ( stop criterion ) influence the solution quality ?